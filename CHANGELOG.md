<div align="center">

<img src="https://gitlab.com/fibric/traily/-/raw/main/Images/logo/logo-small.svg" alt="Traily logo" width="265">

<p style="margin: 0 auto 0">
Carefully crafted adjustments to enhance functionality of removing and invisible characters
</p>

</div>

-----
## 04/25/21 v999.999.999-obsolete

- This extension is now obsolete since Nova 6!
- Nova has already removed invisible characters at line ends when an <code>.editorconfig</code> file was found. Since version 6 Nova removes invisible characters even without an <code>.editorconfig file</code>.

<img src="https://gitlab.com/fibric/traily/-/raw/main/Images/Nova6.1.png" alt="Nova v6.1 screenshot showing settings which enable automatic deletion of invisible characters.">


## v1.0.0

Initial release

### Features

+ Added commands to remove trailing invisible characters
  + `[Cleanup Trailing Invesibles + Newlines]`
  + `[Cleanup Trailing Invisbiles]`
  + `[Cleanup Trailing Newlines]`
+ Allows toggling automatism in extension's preferences
  + `[Automatically Cleanup Trailing Invisible Characters And Newlines]`
    + `[Cleanup Automatically]`
    + `[Invoke Manually]`

-----
<div align="center">
    <img src="https://gitlab.com/fibric/logo/-/raw/master/fibric-logo-text.svg" width="80" alt="Made with ❤️ by fibric">
</div>
